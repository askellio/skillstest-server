from flask import Blueprint, jsonify, request
from models import *
from pony.orm.serialization import to_dict, to_json
from utils import *
generator = Blueprint('generator', __name__)

@generator.route('/api/generate')
@db_session
def generate():
    user1 = User(login="user", password=generate_password("user","user"))
    user2 = User(login="sasha", password=generate_password("sasha", "1234"))

    c1 = Category(name="Мехмат")
    c2 = Category(name="высшая математика", parent=c1)
    c3 = Category(name="дискретная математика", parent=c1)
    c4 = Category(name="прикладная математика", parent=c1)



    test1 = Test(image="http://repetitorfb.ru/wp-content/uploads/2015/07/%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%BD%D1%8B%D0%B9-%D0%92%D0%B0%D1%81%D1%8F.jpg",
                 name="Занимательная математика",
                 description="Данный тест идеально подойдет для детей младших классов",
                 count=3,
                 duration=4*60*1000,
                 category=c4)

    test2 = Test(
        image="http://www.delphis.ru/files/jrnl_body_images/0%281%29_20.09.1993/tri_pritchi_o_pifagore/image001.jpg",
        name="Задачи Пифагора",
        description="Пифагор - один из самых известных античных математиков. Современны люди часто говорят об эволюции. А Вы умнее человека, жившего 1500 лет назад?",
        count=2,
        duration=2 * 60*1000,
        category=c4)

    c5 = Category(name="История")
    c6 = Category(name="Современная история", parent=c5)
    c7 = Category(name="История России", parent=c5)

    test3 = Test(
        image="http://mtdata.ru/u30/photoC9A8/20332631808-0/original.jpg",
        name="СССР",
        description="Тест о обдном из самых недавних периодов нашей истории",
        count=2,
        duration=30*1000,
        category=c7
    )

    q1 = Question(
        test=test1,
        text="Миша купил 3 яблока. Одно съел. Сколько яблок осталось у Миши?"
    )

    var11 = Variant(
        question=q1,
        value="1",
        answer=False
    )

    var12 = Variant(
        question=q1,
        value="0",
        answer = False
    )

    var13 = Variant(
        question=q1,
        value="2",
        answer=True
    )

    var14 = Variant(
        question=q1,
        value="3",
        answer=False
    )

    q2 = Question(
        test=test1,
        text=" Назовите два числа, у которых количество цифр равно количеству букв, составляющих название каждого из этих чисел."
    )

    var21 = Variant(
        question=q2,
        value="300",
        answer=False
    )

    var22 = Variant(
        question=q2,
        value="100",
        answer=True
    )

    var23 = Variant(
        question=q2,
        value="миллион",
        answer=True
    )

    var24 = Variant(
        question=q2,
        value="тыща",
        answer=False
    )

    q3 = Question(
        test=test1,
        text="Когда моему отцу был 31 год, мне было 8 лет, а теперь отец старше меня вдвое. Сколько мне лет теперь?"
    )

    var31 = Variant(
        question=q3,
        value="30",
        answer=False
    )

    var32 = Variant(
        question=q3,
        value="19",
        answer=False
    )

    var33 = Variant(
        question=q3,
        value="24",
        answer=False
    )

    var34 = Variant(
        question=q3,
        value="нет правильного ответа",
        answer=True
    )

    q4 = Question(
        test=test1,
        text="Начнём считать пальцы на правой руке: первый — мизинец, второй — безымянный, третий — средний, четвёртый — указательный, пятый — большой,шестой — снова указательный, седьмой — снова средний, восьмой — безымянный, девятый — мизинец, десятый — безымянный и т. д. Какой палец будет по счёту 1992-м?"
    )

    var41 = Variant(
        question=q4,
        value="мизинец",
        answer=False
    )

    var42 = Variant(
        question=q4,
        value="безымянный",
        answer=True
    )

    var43 = Variant(
        question=q4,
        value="средний",
        answer=False
    )

    var44 = Variant(
        question=q4,
        value="указательный",
        answer=False
    )

    var45 = Variant(
        question=q4,
        value="большой",
        answer=False
    )

    var46 = Variant(
        question=q4,
        value="нет правильного ответа",
        answer=False
    )

    q5 = Question(
        test=test1,
        text=" Пете и Коле купили по коробке конфет. В каждой коробке находится 12 конфет. Петя из своей коробки съел несколько конфет, а Коля из своей коробки съел столько конфет, сколько осталось в коробке у Пети. Сколько конфет осталось на двоих у Пети и Коли?"
    )

    var51 = Variant(
        question=q5,
        value="3",
        answer=False
    )

    var52 = Variant(
        question=q5,
        value="8",
        answer=False
    )

    var53 = Variant(
        question=q5,
        value="10",
        answer=False
    )

    var54 = Variant(
        question=q5,
        value="12",
        answer=True
    )

    q6 = Question(
        test=test1,
        text="Если в 12 часов ночи идет дождь, то можно ли ожидать, что через 72 часа будет солнечная погода?"
    )

    var61 = Variant(
        question=q6,
        value="возможно",
        answer=False
    )

    var62 = Variant(
        question=q6,
        value="невозможно",
        answer=True
    )

    var63 = Variant(
        question=q6,
        value="неизвестно",
        answer=False
    )

    q21 = Question(
        test=test2,
        text="Число в имени Пифагора"
    )

    var211 = Variant(
        question=q21,
        value="2.78",
        answer=False
    )

    var212 = Variant(
        question=q21,
        value="1.34",
        answer=False
    )

    var213 = Variant(
        question=q21,
        value="3.14",
        answer=True
    )

    var214 = Variant(
        question=q21,
        value="4.11",
        answer=False
    )

    q22 = Question(
        test=test2,
        text="Количество нулей в числе гугол"
    )

    var221 = Variant(
        question=q22,
        value="50",
        answer=False
    )

    var222 = Variant(
        question=q22,
        value="70",
        answer=False
    )

    var223 = Variant(
        question=q22,
        value="100",
        answer=True
    )

    var224 = Variant(
        question=q22,
        value="не существует такого числа",
        answer=False
    )

    q23 = Question(
        test=test2,
        text="Радиус Земли был вычислен в "
    )

    var231 = Variant(
        question=q23,
        value="242 год до н.э",
        answer=True
    )

    var232 = Variant(
        question=q23,
        value="35 год н.э.",
        answer=False
    )

    var233 = Variant(
        question=q23,
        value="1697 год н.э.",
        answer=False
    )

    var234 = Variant(
        question=q23,
        value="1964 год н.э.",
        answer=False
    )

    q24 = Question(
        test=test2,
        text="Пифагоровы числа "
    )

    var241 = Variant(
        question=q24,
        value="8, 15, 17",
        answer=True
    )

    var242 = Variant(
        question=q24,
        value="16, 63, 65",
        answer=True
    )

    var243 = Variant(
        question=q24,
        value="31, 113, 148",
        answer=False
    )

    var244 = Variant(
        question=q24,
        value="нет правильного ответа",
        answer=False
    )

    q31 = Question(
        test=test3,
        text="Человек на фотографиях"
    )

    image31= Image(
        url="https://upload.wikimedia.org/wikipedia/commons/c/c5/Bundesarchiv_Bild_183-B0628-0015-035%2C_Nikita_S._Chruschtschow.jpg",
        question=q31
    )
    image32 = Image(
        url="https://24smi.org/public/media/2016/10/28/pic_89d3a604aa5bb4a7e01e6b9d0c34764b.jpg",
        question=q31
    )
    image33 = Image(
        url="http://files.vm.ru/photo/vecherka/2013/09/doc6brwk6hmj29yvel8m0j_800_480.jpg",
        question=q31
    )

    var311 = Variant(
        question=q31,
        value="Ленин",
        answer=False
    )

    var312 = Variant(
        question=q31,
        value="Сталин",
        answer=False
    )

    var313 = Variant(
        question=q31,
        value="Берия",
        answer=False
    )

    var314 = Variant(
        question=q31,
        value="Хрущев",
        answer=True
    )

    q32 = Question(
        test=test3,
        text="Город проведения Олимпиады в 1980 г."
    )

    var321 = Variant(
        question=q32,
        value="Казань",
        answer=False
    )

    var322 = Variant(
        question=q32,
        value="Ростов-на-Дону",
        answer=False
    )

    var323 = Variant(
        question=q32,
        value="Петербург",
        answer=False
    )

    var324 = Variant(
        question=q32,
        value="Москва",
        answer=True
    )

    status1 = TestStatus(
        id = 0,
        value = "выполняется"
    )

    status1 = TestStatus(
        id=1,
        value="завершен"
    )

    status1 = TestStatus(
        id=2,
        value="превышено время выполнения"
    )

    return "good"


# @generator.route('/api/test') #database seed
# @db_session
# def test():
#     persons = select(p for p in Expo)[:]
#     result = {'data': [p.to_dict() for p in persons]}
#     return json.dumps(result)
