import os


from flask import Flask
from generator import generator
from web_api import web_api
import atexit


app = Flask(__name__)
app.register_blueprint(generator)
app.register_blueprint(web_api)

# @app.route("/")
# def hello():
#     return "Hello World!"

if __name__ == '__main__':
    app.config['JSON_AS_ASCII'] = False
    app.run(host="0.0.0.0", port=os.environ.get('PORT', 5000))
    # app.run(host="127.0.0.1", port=os.environ.get('PORT', 5000))
    # app.run(host="192.168.1.35", port=os.environ.get('PORT', 5000))