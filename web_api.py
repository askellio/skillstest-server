from flask import Blueprint, jsonify, request
from models import *
from pony.orm.serialization import to_dict, to_json
from utils import *

web_api = Blueprint('web_api', __name__)


# @web_api.route('/api/categories')
# @db_session
# def getTopCategories():
#     result = []
#     categories = select(c for c in Category if c.parent is None)
#     result = []
#     for c in categories:
#         x = c.to_dict()
#         x["childs"] = len(c.childs)
#         result.append(x)
#     return jsonify(result), 200

@web_api.route('/api/good', methods=['GET'])
@db_session
def good():
    return 'good', 200

@web_api.route('/api/login', methods=['POST'])
@db_session
def login():
    login = request.form['login']
    password = generate_password(login, request.form['password'])

    if User.exists(lambda c: c.login == login and c.password == password):
        cookie = generate_hash('login', rand_str(10))
        user = User.get(login=login)
        user.cookie = cookie
        user.lastLoginDate = datetime.now()
        return cookie, 200
    return 'No such user with these login and password', 404

@web_api.route('/api/categories', methods=['POST'])
@db_session
def getCategories():

    if 'cookie' not in request.headers:
        return 'Access refused! Need authorization via cookie', 401

    cookie = request.headers['cookie']
    user = User.get(cookie=cookie)
    if user is None:
        return 'Access refused! cookie is wrong', 401
    #
    # delta = datetime.now() - user.lastLoginDate
    # if delta.hours>12:
    #     return 'Access refused! cookie is time out', 401

    x = request.form.get('id', -1)

    if str(x).__eq__('-1'):
        categories = select(c for c in Category if c.parent is None)
    else:
        category = Category.get(id=x)
        if category is None:
            return 'There is no category with such id', 404
        else:
            categories = category.childs

    result = []
    for category in categories:
        c = category.to_dict()
        c["childs"] = len(category.childs)
        result.append(c)
    return jsonify(result), 200

@web_api.route('/api/tests', methods=['POST'])
@db_session
def getTests():
    if 'cookie' not in request.headers:
        return 'Access refused! Need authorization via cookie', 401

    cookie = request.headers['cookie']
    user = User.get(cookie=cookie)
    if user is None:
        return 'Access refused! cookie is wrong', 401

    x = request.form['id']
    category = Category.get(id=x)
    if category is None:
        return 'There is no category with such id', 404

    tests = category.tests
    result = []
    for test in tests:
        t = test.to_dict()
        result.append(t)
    return jsonify(result), 200

@web_api.route('/api/start', methods=['POST'])
@db_session
def startTest():

    if 'cookie' not in request.headers:
        return 'Access refused! Need authorization via cookie', 401

    cookie = request.headers['cookie']
    user = User.get(cookie=cookie)
    if user is None:
        return 'Access refused! cookie is wrong', 401

    x = request.form['id']
    test = Test.get(id=x)
    if test is None:
        return 'There is no test with such id', 404

    result = []
    questions = []
    for question in test.questions:
        if len(result) <= test.count:
            q = question.to_dict(exclude='test')
            q['images'] = []
            for image in question.images:
                q['images'].append(image.url)
            q['variants'] = []
            for variant in question.variants:
                q['variants'].append(variant.to_dict(exclude=['answer', 'question', 'result']))
            result.append(q)
            questions.append(question)
        else:
            break;

    test_result = TestResult(
        user=user,
        test=test,
        questions=questions,
        startTime=datetime.now(),
        status=TestStatus.get(id=0)
    )

    # return jsonify(datetime.now()), 200
    return jsonify(result), 200


@web_api.route('/api/finish', methods=['POST'])
@db_session
def temp2():

    test = Test.get(id=request.form['id'])
    cookie = request.headers['cookie']
    user = User.get(cookie=cookie)
    if user is None:
        return 'Access refused! cookie is wrong', 401
    status = TestStatus.get(id=0)

    result = TestResult.select(lambda r: r.user == user and r.test==test and r.status==status).for_update().first()

    if result is None:
        return 'There is no test with such id', 404

    ids = request.form.get('answers', [])

    answers = []
    for id in ids:
        answer = Variant.get(id=id)
        answers.append(answer)

    result.answers = answers
    result.status = TestStatus.get(id=1)


    return "success", 200

@web_api.route('/api/results', methods=['POST'])
@db_session
def results():
    cookie = request.headers['cookie']
    user = User.get(cookie=cookie)
    if user is None:
        return 'Access refused! cookie is wrong', 401
    status = TestStatus.get(id=1)

    results = TestResult.select(lambda r: r.user==user and r.status==status)
    json = []
    for result in results:
        item = dict()
        item['name'] = result.test.name
        item['id'] = result.test.id
        n = 0
        for answer in result.answers:
            if answer.answer:
                n += 1
        item['correct'] = n

        n = len(result.answers) - n

        for question in result.questions:
            for variant in question.variants:
                if variant.answer and variant not in result.answers:
                    n += 1

        item['errors'] = n
        json.append(item)

    return jsonify(json), 200