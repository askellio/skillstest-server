import hashlib
import random, string


def rand_str(length):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length))

def generate_hash(arg1, arg2):
    hash_key = hashlib.md5(str(arg1).encode() + str(arg2).encode())
    return str(hash_key.hexdigest())


def generate_password(arg1, arg2):
    hash_pass = hashlib.sha512(str(arg1).encode() + str('some_salt' + arg2).encode())
    return str(hash_pass.hexdigest())