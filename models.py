# from pony.orm import *
# from datetime import datetime
#
# db = Database()
#
# class User(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     login = Required(str, 50)
#     password = Required (str, 50)
#     cookie = Required(str, 20)
#
#
# class Category(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     parent = Optional(lambda: Category, reverse="childs")
#     childs = Set(lambda: Category, reverse="parent")
#     name = Required(str, 300)
#     tests = Set('Test')
#
# class Test(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     name = Required(str, 300)
#     questions = Set('Question')
#     category = Required(Category)
#
# class Question(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     text = Required(str, 10000)
#     variants = Set('Variant')
#     answers = Set('Answer')
#     test = Required(Test)
#
#
# class Answer(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     value = Required(int)
#     question = Required(Question)
#
# class Variant(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     value = Required(str, 10000)
#     question = Required(Question)
#
# class Expo(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     value = Required(str, 10000)
#     inx = Set("InObject")
#
# class InObject(db.Entity):
#     id = PrimaryKey(int, auto=True)
#     data = Required(Expo)
#     nums = Required(int)
#
#
#
# db.bind('postgres', dbname="drg59br798kgo", user="rggsikggmsjixx",
#         password="50eb245f06d8219303cd3ae35c94cf956ac43c0e3f8ed0c4050c912bd3d9690e",
#         host='ec2-23-23-86-179.compute-1.amazonaws.com', port='5432')
# db.generate_mapping(create_tables=True)


from pony.orm import *
from datetime import datetime

db = Database()

class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    login = Required(str, 50)
    password = Required (str, 128)
    cookie = Optional(str, 128)
    lastLoginDate = Optional(datetime)
    results = Set(lambda: TestResult)


class Category(db.Entity):
    id = PrimaryKey(int, auto=True)
    parent = Optional(lambda: Category, reverse="childs")
    childs = Set(lambda: Category, reverse="parent")
    name = Required(str, 300)
    tests = Set(lambda: Test)

class Test(db.Entity):
    id = PrimaryKey(int, auto=True)
    image = Required(str)
    name = Required(str, 300)
    count = Required(int)
    description = Required(str)
    duration = Required(int)
    questions = Set(lambda: Question)
    category = Required(lambda: Category)
    results = Set(lambda: TestResult)

class Image(db.Entity):
    id = PrimaryKey(int, auto=True)
    url = Required(str)
    question = Required(lambda: Question)

class Question(db.Entity):
    id = PrimaryKey(int, auto=True)
    images = Set(lambda: Image)
    text = Required(str)
    variants = Set(lambda: Variant)
    test = Required(lambda: Test)
    result = Set(lambda: TestResult)

class Variant(db.Entity):
    id = PrimaryKey(int, auto=True)
    value = Required(str, 10000)
    answer=Required(bool)
    question = Required(lambda: Question)
    result = Set(lambda: TestResult)


class TestResult(db.Entity):
    id = PrimaryKey(int, auto=True)
    user = Required(lambda: User)
    test = Required(lambda: Test)
    questions = Set(lambda: Question)
    answers = Set(lambda: Variant)
    startTime = Required(datetime)
    duration = Optional(int)
    status = Required(lambda: TestStatus)

class TestStatus(db.Entity):
    id = PrimaryKey(int, auto=True)
    value = Required(str)
    result = Set(lambda: TestResult)



db.bind('postgres', dbname="drg59br798kgo", user="rggsikggmsjixx",
        password="50eb245f06d8219303cd3ae35c94cf956ac43c0e3f8ed0c4050c912bd3d9690e",
        host='ec2-23-23-86-179.compute-1.amazonaws.com', port='5432')
db.generate_mapping(create_tables=True)